package com.wellington.springvscode.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;

import org.springframework.stereotype.Component;

@Component
public class CacheService {

    private File file;

    public CacheService() {
        this.file = new File("cache");
    }

    private void popCache() throws IOException {
        String line = null;
        int countLine = 0;

        File oldFile = new File("cache.delete");
        this.file.renameTo(oldFile);
        FileReader fileReader = new FileReader(oldFile);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        while ((line = bufferedReader.readLine()) != null) {
            if (countLine  > 2) {
                this.writeText(line);
            }
            countLine++;
        }
        bufferedReader.close();
        oldFile.delete();
    }

    public int search(String number, int times) throws FileNotFoundException, IOException {
        String row;
        int result = -1;
        if(!this.file.canWrite()){
            return result;
        }
        System.out.println();
        FileReader fileReader = new FileReader(this.file);
        BufferedReader br = new BufferedReader(fileReader);
        while ((row = br.readLine()) != null) {
            if (row.indexOf("number:" + number)>=0  && row.indexOf("times:" + times)>=0) {
                result = Integer.parseInt(row.substring(row.indexOf("result:") + 7, row.length()));
            }
        }
        fileReader.close();
        return result;
    }

    public void writeText(String text) throws IOException {
        this.file = new File("cache");
        FileWriter writer = new FileWriter("cache", true);
        FileReader input = new FileReader("cache");
        LineNumberReader linesCount = new LineNumberReader(input);
        long count = linesCount.lines().count();
        String newRow = count > 0 ? "\n" : "";
        if(count >= 10) {
            this.popCache();
        }
        linesCount.close();
        writer.write(newRow + text);
        writer.close();
    }
}