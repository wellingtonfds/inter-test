package com.wellington.springvscode.services;


import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import org.springframework.stereotype.Component;

@Component
public class RsaService {

    public static final String ALGORITHM = "RSA";
    public static final int SIZE = 2048;

    public static final String PATH_PRIVATE_KEY = "";
    public static final String PATH_PUBLIC_KEY = "";

    public KeyPair generateKeys() {
        try {
            final KeyPairGenerator KeyGen = KeyPairGenerator.getInstance(ALGORITHM);
            KeyGen.initialize(SIZE);
            final KeyPair key = KeyGen.generateKeyPair();

            return key;

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public byte[] encrypts(byte[] text, byte[] publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] cipherText = null;
        try {
            PublicKey key = this.castByteToPublicKey(publicKey);
            final Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            cipherText = cipher.doFinal(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cipherText;
    }

    public String decrypts(byte[] text, byte[] privateKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] cipherText = null;
        try {
            PrivateKey key = this.castByteToPrivateKey(privateKey);
            final Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, key);
            cipherText = cipher.doFinal(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String(cipherText);
    }

    public PublicKey castByteToPublicKey(byte[] publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        return KeyFactory.getInstance(ALGORITHM).generatePublic(new X509EncodedKeySpec(publicKey));
    }

    public PrivateKey castByteToPrivateKey(byte[] privateKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        return KeyFactory.getInstance(ALGORITHM).generatePrivate(new PKCS8EncodedKeySpec(privateKey));
    }
}