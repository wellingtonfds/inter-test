package com.wellington.springvscode.services;

import java.io.IOException;

import com.wellington.springvscode.model.User;
import com.wellington.springvscode.model.UserNumberRepository;
import com.wellington.springvscode.model.UserNumbers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UniqueNumberService {

    @Autowired
    private CacheService cache;

    @Autowired
    private UserNumberRepository userNumberRepository;

    public UniqueNumberService(UserNumberRepository userNumberRepository) {
        this.userNumberRepository = userNumberRepository;
    }

    public int unique(UserNumbers userNumbers, User user) {

        UserNumbers newUserNumber = new UserNumbers();
        newUserNumber.setNumbers(userNumbers.getNumbers());
        newUserNumber.setTimes(userNumbers.getTimes());
        String number = userNumbers.getNumbers();
        int times = userNumbers.getTimes();
        if (times == 0) {
            times = 1;
        }
        try {
            int cacheResult = this.cache.search(number, times);
            if (cacheResult >= 0) {
                newUserNumber.setResult(cacheResult);
            } else {
                String[] numberList = number.split("");
                int total = 0;
                for (final String ch : numberList) {
                    total += Integer.parseInt(ch);
                }
                total *= times;
                this.cache.writeText("number:" + number + ",times:" + times + ",result:" + total);
                newUserNumber.setResult(total);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        newUserNumber.setUserId(user.getId());
        userNumberRepository.save(newUserNumber);
        return userNumbers.getResult();
    }

    public Iterable<UserNumbers> list(User user){
        return this.userNumberRepository.findByUserIdIterable(user);
    }
}