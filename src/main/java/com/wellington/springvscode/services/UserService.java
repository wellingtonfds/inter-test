package com.wellington.springvscode.services;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;

import java.util.Base64;

import java.util.Optional;

import com.wellington.springvscode.model.User;
import com.wellington.springvscode.model.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RsaService rsaService;

    public UserService(UserRepository userRepository, RsaService rsaService) {
        this.userRepository = userRepository;
        this.rsaService = rsaService;
    }

    public Iterable<User> list() {
        return this.userRepository.findAll();
    }

    public User update(Long id, User user, byte[] publicKey) throws InvalidKeySpecException, NoSuchAlgorithmException {
        User hasUser = this.show(id, publicKey);
        if (hasUser.getEmail() != null) {
            hasUser.setEmail(rsaService.encrypts(user.getEmail(), hasUser.getPublicKey()));
            hasUser.setName(rsaService.encrypts(user.getName(), hasUser.getPublicKey()));
            this.userRepository.save(hasUser);
            return hasUser;
        }
        return null;
    }

    public User create(User user) throws InvalidKeySpecException, NoSuchAlgorithmException {
        KeyPair keys = rsaService.generateKeys();
        user.setPrivateKey(keys.getPrivate().getEncoded());
        user.setPublicKey(keys.getPublic().getEncoded());
        user.setEmail(rsaService.encrypts(user.getEmail(), user.getPublicKey()));
        user.setName(rsaService.encrypts(user.getName(), user.getPublicKey()));
        this.userRepository.save(user);
        return user;
    }

    public boolean auth(User user, byte[] publicKey) {

        try {
            PublicKey userKey = rsaService.castByteToPublicKey(user.getPublicKey());
            PublicKey pubKey = rsaService.castByteToPublicKey(Base64.getDecoder().decode(publicKey));
            return userKey.equals(pubKey);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public void delete(Long id, byte[] publicKey) {
        User hasUser = this.show(id, publicKey);
        this.userRepository.delete(hasUser);
    }

    public User show(Long id, byte[] publicKey) {
        Optional<User> hasUser = this.userRepository.findById(id);
        User user = new User();
        if (hasUser.isPresent()) {
            user = hasUser.get();
            try {
                if (this.auth(user, publicKey)) {
                    user.setName(rsaService.decrypts(user.getName(), user.getPrivateKey()));
                    user.setEmail(rsaService.decrypts(user.getEmail(), user.getPrivateKey()));
                } else {
                    user = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return user;
    }
}
