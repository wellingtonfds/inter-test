package com.wellington.springvscode.resources;

import javax.validation.Valid;

import com.wellington.springvscode.model.User;
import com.wellington.springvscode.model.UserNumbers;
import com.wellington.springvscode.services.UniqueNumberService;
import com.wellington.springvscode.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@RestController
@RequestMapping("number")
public class NumberResources {

    @Autowired
    private UniqueNumberService uniqueNumberService;

    @Autowired
    private UserService userService;

    @PostMapping("/{id}")
    @ResponseBody
    public int uniqueNumber(@Valid @RequestBody UserNumbers userNumbers, @RequestHeader("publicKey") byte[] publicKey,
            @PathVariable("id") Long id) {
        User user = this.userService.show(id, publicKey);
        if (user != null) {
            int result = uniqueNumberService.unique(userNumbers, user);
            return result;
        }

        return 0;
    }
    @GetMapping("{id}")
    @ResponseBody
    public Iterable<UserNumbers> list(@RequestHeader("publicKey") byte[] publicKey, @PathVariable("id") Long id) {
        User user = this.userService.show(id, publicKey);
        if (user != null) {
            return this.uniqueNumberService.list(user);
        }
        return null;
    }

}