package com.wellington.springvscode.resources;


import javax.validation.Valid;

import com.wellington.springvscode.model.User;
import com.wellington.springvscode.services.RsaService;
import com.wellington.springvscode.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.minidev.json.JSONObject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@RestController
@RequestMapping("user")
public class UserResource {

    @Autowired
    private UserService userService;

    public UserResource(UserService userService, RsaService rsaService) {
        this.userService = userService;
    }

    @GetMapping
    @ResponseBody
    public Iterable<User> list() {
        return this.userService.list();
    }

    @PostMapping
    @ResponseBody
    @ResponseStatus(code = HttpStatus.CREATED)
    public User create(@Valid @RequestBody User user) {
        try {
            return this.userService.create(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    @PutMapping("{id}")
    @ResponseBody
    public User update(@PathVariable("id") Long id, @Valid @RequestBody User user,
            @RequestHeader("publicKey") byte[] publicKey) {
        try {
            return this.userService.update(id, user, publicKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping("show/{id}")
    @ResponseBody
    public JSONObject show(@PathVariable("id") Long id, @RequestHeader("publicKey") byte[] publicKey) {
        try {
            User user = this.userService.show(id, publicKey);
            if (user == null) {
                return null;
            }
            JSONObject objectNode = new JSONObject();
            objectNode.put("name", new String(user.getName()));
            objectNode.put("email", new String(user.getEmail()));
            return objectNode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id, @RequestHeader("publicKey") byte[] publicKey) {
        this.userService.delete(id, publicKey);
    }
}