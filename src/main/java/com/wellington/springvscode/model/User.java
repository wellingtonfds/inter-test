package com.wellington.springvscode.model;



import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Entity;

import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "Users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Lob
    @NotNull(message = "Please fill the name")
    private byte[] name;

    @Lob
    @NotNull(message = "Please fill the email")
    private byte[] email;

    @Lob
    @JsonIgnore
    private byte[] privateKey;

    @Lob
    private byte[] publicKey;

    // @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    // @Cascade(value = CascadeType.ALL)
    // private List<UserNumbers> listUserNumbers = new ArrayList<UserNumbers>();

    public byte[] getEmail() {
        return email;
    }
    // public List<UserNumbers> getlistUserNumbers() {
    //     return listUserNumbers;
    // }

    // public void setlistUserNumbers(List<UserNumbers> listUserNumbers) {
    //     this.listUserNumbers = listUserNumbers;
    // }

    public byte[] getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(byte[] publicKey) {
        this.publicKey = publicKey;
    }

    public byte[] getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(byte[] privateKey) {
        this.privateKey = privateKey;
    }

    public byte[] getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.getBytes();
    }

    public void setName(byte[] name) {
        this.name = name;
    }
    public void setEmail(String email) {
        this.email = email.getBytes();
    }
    public void setEmail(byte[] email) {
        this.email = email;
    }
    public Long getId(){
        return this.id;
    }
    public void setId(Long id){
        this.id = id;
    }
    
    
}