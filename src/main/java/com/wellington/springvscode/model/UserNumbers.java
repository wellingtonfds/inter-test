package com.wellington.springvscode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import org.springframework.lang.Nullable;

@Entity
@Table(name = "UserNumbers")
public class UserNumbers {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(length = 255)
    private String numbers;

    private int times;

    @Nullable
    private int result;

    @Nullable
    private Long userId;



    public String getNumbers() {
        return numbers;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setNumbers(String numbers) {
        this.numbers = numbers;
    }

    public int getTimes() {
        return times;
    }

    public void setTimes(int times) {
        this.times = times;
    }

    public int getResult(){
        return this.result;
    }

    public void setResult(int result){
        this.result = result;
    }
    

}