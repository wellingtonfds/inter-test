package com.wellington.springvscode.model;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserNumberRepository extends JpaRepository<UserNumbers, Long> {
    public Optional<UserNumbers> findById(Long id);
    public List<UserNumbers> findByUserId(User user);
}