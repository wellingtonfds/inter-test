package com.wellington.springvscode.model;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
 
    public User findByName(String name);
    public User findByPublicKey(byte[] publicKey);
    public Optional<User> findById(Long id);
    

}